"""
Contains all parameters and random number generators
"""

import numpy as np
import random as rnd

## PRINT ALGORITHM'S PROGRESS
VERBOSE = True


## POLICY PROPERTIES
# Risk estimator
R_MIN = 0                                               # Minimum risk value
R_MAX = 4                                               # Maximum risk value
R_N = 5                                                 # Number of risk bins
R_GRID = [round(num, 2) for num in np.linspace(R_MIN, R_MAX, R_N)]

# Age dimension
AGE_MIN = 55                                            # Starting age screening
AGE_MAX = 70                                            # Stopping age screening
AGE_N = 4                                               # Number of age groups
AGE_GRID = np.linspace(AGE_MIN, AGE_MAX, AGE_N)

# Feasible screening intervals
INTERVALS = (3, 2, 1)                                   # Intervals in the action set A


## EVOLUTIONARY ALGORITHM SETTINGS
# Mutations
MUTPB = 0.3                                             # p_M -> Mutation rate
PERC_MUT = 0.6                                          # p_e -> % of ages of mutant to be mutated
NR_INITIAL_MUTATIONS = int(PERC_MUT * AGE_N)            # Absolute number of ages to be mutated
NR_MUTATIONS = int(PERC_MUT * AGE_N)

# Population properties
SEL_SIZE = 200                                          # N_sel
POP_SIZE = 2*SEL_SIZE                                   # N_pop = 2*N_sel

# Stopping criterion
STOP = 30                                               # N_stop


## COSTS AND QALYS
# Reference strategy -> No screening situation      #TODO randomize
REF_STRATEGY_QALYS = 62.205243
REF_STRATEGY_COSTS = 3605.950573

## Hypervolume reference point
REF_HV_QALYS = 0
REF_HV_COSTS = 4000


## SEEDS / RANDOM NUMBER GENERATORS
RNG_INITIALIZATION = rnd.Random(2)
RNG_MATING_OP = rnd.Random(3)
RNG_MUTATION_OP = rnd.Random(5)
RNG_MUTATION_PB = rnd.Random(8)
RNG_SELECTION_OP = rnd.Random(9)
