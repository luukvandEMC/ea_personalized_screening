from datetime import datetime

from deap import base, creator, tools
import pandas as pd

from algorithms.HallOfFame import ParetoFront
from algorithms.NSGAII import selectNSGA2, calcHypervolume
import algorithms.settings as settings
from policies import Policy


def main(evaluation_function):
    """
    Runs the Evolutionary Algorithm
    """
    ## Set up settings of DEAP package
    # Minimize costs, maximize QALYs
    creator.create("FitnessMult", base.Fitness, weights=(-1.0, 1.0))
    creator.create("Individual", Policy, fitness=creator.FitnessMult)

    toolbox = base.Toolbox()
    toolbox.register("individual", creator.Individual, intervals=settings.INTERVALS)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    # Define the operators
    # Mating operator
    toolbox.register("mate", Policy.mate_kpoint)
    # Mutation operator
    toolbox.register("mutate", Policy.mut_bounded, nr_mutations=settings.NR_MUTATIONS)
    # Ranking by NSGA-II algorithm
    toolbox.register("rank", selectNSGA2)
    # Random selection for tournament selection algorithm
    toolbox.register("select_rnd", tools.selRandom, k=2)
    # Fitness evaluation
    toolbox.register("evaluate", evaluation_function)

    # Define statistics to track - Size of the memory
    stats = tools.Statistics()
    stats.register("len", len)
    logbook = tools.Logbook()

    # Set starting time
    start_time = datetime.now()
    timestamp = start_time.strftime('%H:%M:%S')
    string = f"Starts initialization at {timestamp}"
    if settings.VERBOSE:
        print(string)

    # Initialize empty memory
    hall_of_fame = ParetoFront()
    hall_of_fame_ext = ParetoFront(extended_dominance=True)

    # Initialize population with N_pop random policies
    pop = toolbox.population(n=settings.POP_SIZE)
    for ind in pop:
        fit = toolbox.evaluate(ind)
        ind.fitness.values = fit

    # Update memory with initial population
    pof_added, pof_removed = hall_of_fame.update(pop)
    hall_of_fame_ext.update(pop)

    # Calculate initialization time
    end_time = datetime.now()
    run_time = end_time - start_time
    start_time = end_time
    timestamp = start_time.strftime('%H:%M:%S')
    string = f"Run time = {run_time}\nStarts algorithm at {timestamp}"
    if settings.VERBOSE:
        print(string)

    # Update logbook
    record = stats.compile(hall_of_fame)
    # Log Hypervolume
    record["EHV"] = calcHypervolume(hall_of_fame_ext)
    # Log number of policies added to/removed from Pareto Frontier in this iteration
    record["memory_added"] = pof_added
    record["memory_removed"] = pof_removed
    # Log running time
    record["Sim_time"] = str(run_time)
    logbook.record(gen=-1, **record)

    # Initialize stopping criterion
    stopping_count = 0
    stop = False
    g = -1

    # START ALGORITHM
    while not stop:
        g += 1
        del record

        # Log time iteration starts
        start_time = datetime.now()
        timestamp = start_time.strftime('%H:%M:%S')
        string = f"Starts iteration {g} at {timestamp}"
        if settings.VERBOSE:
            print(string)

        # Select parents for mating pool
        mating_pool = []
        # Elitist selection phase
        mating_pool.extend(hall_of_fame_ext)
        remaining_number_to_select = settings.SEL_SIZE - len(mating_pool)
        if remaining_number_to_select < 0:
            mating_pool = settings.RNG_SELECTION_OP.sample(mating_pool, settings.SEL_SIZE)
            remaining_number_to_select = 0

        # Tournament selection phase, based on rank and crowding distance
        toolbox.rank(pop)
        for _ in range(remaining_number_to_select):
            (parent1, parent2) = toolbox.select_rnd(pop)

            # Two policies with equal rank, choose one with highest crowding distance
            if parent1.fitness.rank == parent2.fitness.rank:
                if parent1.fitness.crowding_dist > parent2.fitness.crowding_dist:
                    mating_pool.append(parent1)
                else:
                    mating_pool.append(parent2)

            # Two policies with unequal rank, choose one with lowest rank
            elif parent1.fitness.rank < parent2.fitness.rank:
                mating_pool.append(parent1)
            elif parent1.fitness.rank > parent2.fitness.rank:
                mating_pool.append(parent2)
            else:
                raise ValueError
        offspring = (list(map(toolbox.clone, mating_pool)))

        # Apply cross-over
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            toolbox.mate(child1, child2)
            del child1.fitness.values       # Delete fitness values, they are updated later
            del child2.fitness.values

        # Apply mutations
        for mutant in offspring:
            if settings.RNG_MUTATION_PB.random() < settings.MUTPB:
                toolbox.mutate(mutant)

        # Log running time
        end_time = datetime.now()
        run_time = end_time - start_time
        start_time = end_time
        timestamp = start_time.strftime('%H:%M:%S')
        string = f"Run time = {run_time}\nStarts simulation of offspring {g} at {timestamp}"
        if settings.VERBOSE:
            print(string)
        alg_time = str(run_time)

        # Evaluate costs and QALYs of offspring
        for ind in offspring:
            fit = toolbox.evaluate(ind)
            ind.fitness.values = fit

        # Log running time
        end_time = datetime.now()
        run_time = end_time - start_time
        sim_time = str(run_time)
        string = f"Run time = {run_time}"
        if settings.VERBOSE:
            print(string)

        # Update population
        mating_pool.extend(offspring)
        pop[:] = mating_pool

        # Update Pareto front
        pof_added, pof_removed = hall_of_fame.update(pop)
        pof_added_ext, pof_removed_ext = hall_of_fame_ext.update(pop)

        # Log stats
        record = stats.compile(hall_of_fame)
        record["EHV"] = calcHypervolume(hall_of_fame_ext)
        record["memory_added"] = pof_added
        record["memory_removed"] = pof_removed
        record["Alg_time"] = alg_time
        record["Sim_time"] = sim_time
        logbook.record(gen=g, **record)

        if settings.VERBOSE:
            print("Nr. added strategies = " + str(pof_added))
            print("Hypervolume = " + str(record["EHV"]))

        # Check stopping criterion
        if pof_added == 0:
            stopping_count += 1
            if stopping_count == settings.STOP:
                stop = True
        else:
            stopping_count = 0


    ## OUTPUT
    # Output stats
    df = pd.DataFrame(logbook)
    df.to_csv(f"output/stats.csv")

    # Output of population and memory
    columns = list(settings.INTERVALS)
    columns.extend(["costs", "QALYs"])
    pop_output = pd.DataFrame(columns=columns)
    ix = 0
    for ind in pop:
        ix += 1
        column = list(ind.edges.values())
        column.extend(ind.fitness.values)
        pop_output.loc[ix] = column

    pof_output = pd.DataFrame(columns=columns)
    ix = 0
    for ind in hall_of_fame:
        ix += 1
        column = list(ind.edges.values())
        column.extend(ind.fitness.values)
        pof_output.loc[ix] = column

    pop_output.to_csv(f"output/population.csv")
    pof_output.to_csv(f"output/memory.csv")
