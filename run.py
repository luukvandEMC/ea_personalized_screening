import pickle

from data.evaluate import evaluate_policy_with_data_factory
from algorithms.evolutionary_algorithm import main as main_evolutionary_algorithm

# Import the benchmark data file
with open(r"data\data.pickle", 'rb') as file:
    data = pickle.load(file)
eval_func = evaluate_policy_with_data_factory(data)

# Run the algorithm
main_evolutionary_algorithm(evaluation_function=eval_func)

print("Done! Find the output in the output folder.")