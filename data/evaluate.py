import numpy as np
from typing import Dict

from algorithms import settings as settings
from policies import Policy


def evaluate_policy_with_data_factory(data: Dict):
    """
    Initializes the evaluation function with the imported data set.
    :param data: Imported data set
    :return: An initialized evaluation function
    """
    benchmark_dict = data

    def evaluate_policy_with_data(policy: Policy):
        """
        Extracts the costs and QALYs of a policy from the database. Converts the policy into a
        unique ID. The database contains fictional costs/QALYs for each ID.
        :param policy: The policy to evaluate.
        :return: Returns the (fictional) costs and QALYs of this policy, with respect to a no
            screening scenario.
        """
        pol_ints = {}
        for interval, edge_mult in policy.edges.items():
            edge_int = 0
            idx = 0
            for elt in edge_mult.get_y_list():
                edge_int += elt * settings.R_N ** idx
                idx += 1
            pol_ints[interval] = edge_int

        pol_val = 1e6 * pol_ints[3] + 1e3 * pol_ints[2] + pol_ints[1]
        ce = np.array(benchmark_dict[pol_val]) - np.array(
            (settings.REF_STRATEGY_COSTS, settings.REF_STRATEGY_QALYS)).tolist()
        return ce

    return evaluate_policy_with_data
