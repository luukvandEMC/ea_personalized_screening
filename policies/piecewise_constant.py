from typing import Sequence, SupportsFloat, Tuple

import numpy as np
import pandas as pd


class PiecewiseConstant:
    """
    A piecewise constant function. Can be used as follows:

    >>> data = [(0, 3.0), (0.2, 4.0), (1.0, 5.5)]
    >>> pc = PiecewiseConstant(data)
    >>> pc(0.1, 1.5)
    3.0

    See https://en.wikipedia.org/wiki/Step_function for further details.

    For a given action set ``A``, a screening policy is characterized by ``|A|-1``
    PiecewiseConstant objects that represent the "policy bounds".

    :param data: The data to construct the piecewise constant function with.
        Should be a sequence of sequences. The latter sequences should each
        have three elements: an x-value, y-value and z-value. Thus, an Nx3 matrix
        is valid input.
    :raises AssertionError: PiecewiseConstant should be initialized with a
        sequence.
    :raises AssertionError: Each entry should be a sequence.
    :raises AssertionError: Each entry should have exactly two elements.
    :raises AssertionError: Each value should be (convertible to) a float.
    :raises AssertionError: x should be non-decreasing.
    """

    Data = Sequence[Tuple[SupportsFloat, SupportsFloat, SupportsFloat]]
    """Type hinting for the input data that can be used to create a
    :class:`PiecewiseConstant` object.
    """

    def __init__(self, data: Data):
        # Check if input data is a sequence
        assert hasattr(data, "__iter__"), \
            "PiecewiseConstant should be initialized with a sequence."

        # Input should have at least one entry
        assert len(data) > 0

        # Check each entry
        for i in data:
            assert hasattr(data, "__iter__"), \
                "Each entry should be a sequence."
            assert len(i) == 2, \
                "Each entry should have exactly two elements."
            assert all(hasattr(j, "__float__") for j in i), \
                "Each value should be (convertible to) a float."

        # Convert input data to NumPy array
        self.data = np.array(data, dtype=np.float64)

        # Store x and y columns separately
        self._x = np.array(self.data[:, 0], dtype=np.float64)
        self._y = np.array(self.data[:, 1], dtype=np.float64)

        # Check if x is non-decreasing
        self.check_feasibility()

        # Store number of entries
        self._n = self._x.shape[0]

    def check_feasibility(self):
        assert np.all(np.diff(self._x) >= 0.), "x should be non-decreasing."

    def __call__(self, x: float) -> float:
        """
        Evaluate a single value.

        :param x: The value to evaluate.
        :return: The obtained value.
        """
        if x <= self._x[0]:
            return self._y[0]

        for idx in range(1, self._n):
            if x < self._x[idx]:
                return self._y[idx - 1]

        return self._y[self._n - 1]

    def mate_breaks_2d(self, other, break_x):
        """
        Method for k-point cross-over (k=2). Exchanges the y values of two PiecewiseConstants
            between the two x coordinates in break_x.
        :param other: Other policy.
        :param break_x: List containing the two x breakpoints between which the y values are
            exchanged.
        :return:
        """
        this_points, other_points = list(self.get_xy_iter()), list(other.get_xy_iter())
        idx = len(this_points)-1

        # Loop over all x points. Exchange y values if the x is between the crossover points.
        while idx > -1:
            _x, this_y = this_points[idx]
            other_x, other_y = other_points[idx]
            assert _x == other_x, "Both PiecewiseConstants must have the same x domain."

            if _x < break_x[0]:
                return

            if break_x[0] <= _x <= break_x[1]:
                self.set_y(idx, other_y)
                other.set_y(idx, this_y)
            idx -= 1
        return

    def get_xy_iter(self):
        return zip(self._x, self._y)

    def set_yx(self):
        return set(zip(self._y, self._x))

    def get_x_list(self):
        return np.asarray(self._x)

    def get_y_list(self):
        return np.asarray(self._y)

    def set_y(self, idx, new_y):
        self._y[idx] = new_y

    def __str__(self):
        dic = {x: y for x, y in self.get_xy_iter()}
        return str(pd.DataFrame(dic))

    def __eq__(self, other):
        return all(self._y == other._y)

    def __getstate__(self):
        return tuple(zip(self._x, self._y))

    def __setstate__(self, data):
        self.__init__(data)

    def to_df(self):
        dic = {x: y for x, y in self.get_xy_iter()}
        return pd.DataFrame(dic)
