from policies.piecewise_constant import PiecewiseConstant
from algorithms import settings as settings
import numpy as np


class Policy:
    """
    Class that represents a screening policy. If ``A`` is the action set, it contains ``|A|-1``
    PiecewiseConstant objects, each representing a policy bound. Can be used as follows:

    >>> intervals = (3,2,1)
    >>> data = {3: [(55, 0.2), (60, 0.7), (65, 0.3), (70, 0.5)],
    >>>         2: [(55, 0.5), (60, 0.7), (65, 0.6), (70, 0.7)],
    >>>         1: [(55, 0.9), (60, 0.9), (65, 0.8), (70, 1.0)]}
    >>> pol = Policy(intervals, data)
    >>> pol(2)(60)
    0.7

    :param intervals: The feasible screening intervals. I.e. the complete action set ``A`` except
        for the action COL.
    :param data: A dictionary containing the data for each of the policy bounds that belong to each
        interval. If empty, the Policy is initialized randomly.
    :raises AssertionError: The data must be presented as a dictionary.
    :raises AssertionError: The list of intervals must be a decreasing list
    :raises AssertionError: No policy was specified for the interval '{interval}'
    :raises AssertionError: For every interval, the input should be (convertible to) a
        PiecewiseConstant
    """

    def __init__(self, intervals: tuple, data=None):
        # If no input data was given, generate trivial policies first
        if data is None:
            data = init_policies_constant_full(intervals=intervals)
            do_mutations = True
        else:
            do_mutations = False
            assert type(data) == dict, "The data must be presented as a dictionary"

        self.intervals = intervals
        self.edges = {}

        # Check if the new policies are feasible
        prev_interval = float("inf")
        for interval in intervals:
            # Check input intervals
            assert interval < prev_interval, "The list of intervals must be a decreasing list"
            assert interval in data, f"No policy was specified for the interval '{interval}'"
            prev_interval = interval

            # Convert the data into a PiecewiseConstant for each action
            if (type(data[interval]) == list) or (type(data[interval]) == tuple):
                self.edges[interval] = PiecewiseConstant(data[interval])
            else:
                assert type(data[interval]) == PiecewiseConstant, \
                    "For every interval, the input should be (convertible to) a PiecewiseConstant"
                self.edges[interval] = data[interval]

        # If the policy was initialized trivially, apply mutations
        if do_mutations:
            self.mut_bounded(settings.NR_INITIAL_MUTATIONS)
        self.check_feasibility()

    def check_feasibility(self):
        """
        Checks if the policy is feasible. It is if (1) the x-domains of all policy bounds are equal
            and (2) it adheres to the impact ordering assumption.
        """
        for idx in range(1, len(self.intervals)):
            curr_edge = self.edges[self.intervals[idx-1]]
            next_edge = self.edges[self.intervals[idx]]
            curr_x = np.array(curr_edge.get_x_list())
            next_x = np.array(next_edge.get_x_list())
            curr_y = np.array(curr_edge.get_y_list())
            next_y = np.array(next_edge.get_y_list())

            # Check if first PiecewiseConstant is feasible
            if idx == 1:
                curr_edge.check_feasibility()

            # Check if the x domains of all PiecewiseConstants for all intervals are equal
            if not np.array_equal(curr_x, next_x):
                raise ValueError(
                    f"Domains of {self.intervals[idx - 1]} and {self.intervals[idx]} are not equal"
                )

            # Check y-values (impact ordering assumption)
            if not np.all(next_y >= curr_y):
                loc = next_y[np.where(next_y - curr_y < 0)]
                raise ValueError(
                    f"The impact ordering assumption does not hold at {loc} in\n{self}"
                )

    def mate_kpoint(self, other):
        """
        Apply k_point mutation with k=2.
        :param other: Other policy with which to be mated.
        """
        # Randomly select two ages which are the break point of the mutation
        break_x = sorted(settings.RNG_MATING_OP.choices(settings.AGE_GRID, k=2))

        # Mutate each PiecewiseConstant in this policy between the coordinates in break_x
        for interval in self.intervals:
            self(interval).mate_breaks_2d(other.edges[interval], break_x)

        self.check_feasibility()
        other.check_feasibility()

    def mut_bounded(self, nr_mutations: int):
        """
        Apply mutations
        :param nr_mutations: Number of mutations to be performed within a policy
        """
        nr_of_intervals = len(self.intervals)

        # Choose elements to be mutated
        locations_x = settings.RNG_MUTATION_OP.choices(range(settings.AGE_N), k=nr_mutations)
        locations_x = np.unique(locations_x)

        for location in locations_x:
            # Determine if new y values are drawn above a lower or below an upper bound
            is_upper_bound = (settings.RNG_MUTATION_OP.random() < 0.5)
            if is_upper_bound:
                bound = settings.RNG_MUTATION_OP.choice(range(1, settings.R_N))
                new_y_values = settings.RNG_MUTATION_OP.choices(population=settings.R_GRID[:bound],
                                                                k=nr_of_intervals)
            else:
                bound = settings.RNG_MUTATION_OP.choice(range(settings.R_N - 1))
                new_y_values = settings.RNG_MUTATION_OP.choices(population=settings.R_GRID[bound:],
                                                                k=nr_of_intervals)

            # Sort such that the mutation policy adheres to the impact ordering assumption
            new_y_values.sort()

            # Apply the mutations to the PiecewiseConstants
            for interval, new_y_value in zip(self.intervals, new_y_values):
                self(interval).set_y(location, new_y_value)

        self.check_feasibility()

    def __call__(self, edge):
        if edge not in self.intervals:
            raise Exception(f"The edge {edge} is not in this policy")
        return self.edges[edge]

    def __str__(self):
        res = ""
        for interval in self.intervals:
            res += str(interval) + ": " + str(self.edges[interval]) + "\n"
        return res

    def __sub__(self, other):
        res = {}
        for (interval, this), (_, oth) in zip(self.edges, other.edges):
            res[interval] = this-oth
        return res

    def __add__(self, other):
        res = {}
        for (interval, this), (_, oth) in zip(self.edges, other.edges):
            res[interval] = this + oth
        return res

    def __truediv__(self, other):
        res = {}
        for (interval, this), (_, oth) in zip(self.edges, other.edges):
            res[interval] = this / oth
        return res

    def __eq__(self, other):
        if np.any(self(self.intervals[0]).get_x_list() != other(other.intervals[0]).get_x_list()):
            return False
        for interval in self.intervals:
            if np.any(self(interval).get_y_list() != other(interval).get_y_list()):
                return False
        return True


def init_policies_constant_full(intervals: tuple):
    """
    Produces trivial policies in which all policy borders are constant.
    :param intervals: List of intervals in the policy.
    :return: A list of new policies.
    """
    nr_of_intervals = len(intervals)
    b_values = settings.RNG_INITIALIZATION.choices(population=settings.R_GRID, k=nr_of_intervals)
    b_values.sort()
    new_policy_data = {
        intervals[idx]: [(age, b_values[idx]) for age in settings.AGE_GRID] for
        idx in range(0, nr_of_intervals)
    }
    return new_policy_data
