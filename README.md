Repository that contains example code for the Evolutionary Algorithm to personalize screening strategies. 
The corresponding publication can be found here: https://www.frontiersin.org/articles/10.3389/fphys.2021.718276/.
This repository contains the following files:

- run.py -> From here the algorithm can be run

- algorithms/evolutionary_algorithm.py -> This code brings all operators together and forms the EA.
- algorithms/HallOfFame.py -> Specifies the ParetoFronts and memory of the algorithm. Code is based on the module from the DEAP package.
- algorithms/NSGAII.py -> Specifies the selection operator. Code is based on the module from the DEAP package.
- algorithms/settings.py -> Specifies all (hyper)parameters for the algorithm and screening policies.

- data/ -> Contains a fictional benchmark dataset with fictional costs and QALYs specified for all feasible policies in Case 1 of the manuscript.

- policies/piecewise_constant.py -> Defines the PiecewiseConstant class of which the policy edges are made up. Contains the mutation and cross-over operators.
- policies/policy.py -> Defines the Policy class. Contains the mutation and cross-over operators.

NOTE that all costs and effects represented in the benchmark set and settings.py are fictional. No rights can be derived from any of the values regarding the feasibility of screening programmes. This data is published to serve as an example for the algorithm.
